//
//  ApiManager.swift
//  newsFeed
//
//  Created by pooja on 07/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import UIKit

protocol apiManagerDelegate: class {
    func apiSuccessResponse(_ response: Dictionary<String,Any>)
    func apiFailureResponse(_ msgError: String)
}

class ApiManager: NSObject {
    
    weak var delegate: apiManagerDelegate?
    
    //MARK: Api call to fetch data from server
    func FetchDataFromApi(url: String) {
        var request = URLRequest(url: URL(string: url)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = nil
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error.debugDescription)
                self.delegate?.apiFailureResponse("Please try again")
            } else {
                let responseStrInISOLatin = String(data: data!, encoding: String.Encoding.isoLatin1)
                guard let modifiedDataInUTF8Format = responseStrInISOLatin?.data(using: String.Encoding.utf8) else {
                    print("could not convert data to UTF-8 format")
                    return
                }
                do {
                    let responseJSONDict = try JSONSerialization.jsonObject(with: modifiedDataInUTF8Format) as! [String:Any]
                    self.delegate?.apiSuccessResponse(responseJSONDict)
                } catch {
                    print(error)
                }
            }
        })
        dataTask.resume()
    }
}
