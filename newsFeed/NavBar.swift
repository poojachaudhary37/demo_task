//
//  NavBar.swift
//  newsFeed
//
//  Created by pooja on 11/02/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation
import UIKit

protocol NavBar {
    func setNavigationBarProperties()
}
//MARK: To set navigation title
extension NavBar where Self:UIViewController {
    func setNavigationBarProperties() {
        let bar = self.navigationController?.navigationBar
        bar?.barTintColor = UIColor.navBarTintColor
        bar?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.navBarTitleColor]
        bar?.isTranslucent = false
    }
}
