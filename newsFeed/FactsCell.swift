//
//  FactsCell.swift
//  newsFeed
//
//  Created by pooja on 07/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import UIKit
import SDWebImage

class FactsCell: UITableViewCell {

    lazy var mainView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.tableViewMainViewBackgroundColor
        return view
    }()
    //MARK: to set title label to the view
    lazy var titleLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = UIColor.titleLabelTextColor
        label.numberOfLines = 0
        label.setContentCompressionResistancePriority(UILayoutPriority.init(rawValue: 751), for: .vertical)
        return label
    }()
    //MARK: to set description label to the view
    lazy var descLabel:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        label.setContentHuggingPriority(UILayoutPriority(rawValue: 249), for: UILayoutConstraintAxis.horizontal)
        return label
    }()
    //MARK: to set image view
    lazy var factImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    lazy var nextArrowImageView:UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = #imageLiteral(resourceName: "next")
        return imageView
    }()
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        setupUIElements()
        setupLayoutConstraints()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    var dataSource:FactsData?{didSet{
        if let source = dataSource {
            self.titleLabel.text = source.title
            self.descLabel.text = source.description
            self.factImageView.setIndicatorStyle(UIActivityIndicatorViewStyle.white)
            self.factImageView.setShowActivityIndicator(true)
            if source.imageURL == "" {
                self.factImageView.sd_setImage(with: URL(string: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8EX9_Hyu5OsPcYNBdYxBVAZtXNdGrOiKyVNgAXkMSbALWEocG_w"))
            }else {
                self.factImageView.sd_setImage(with: URL(string: source.imageURL))
            }
        }
        }}
    private func setupUIElements() {
        mainView.addSubview(titleLabel)
        mainView.addSubview(descLabel)
        mainView.addSubview(factImageView)
        mainView.addSubview(nextArrowImageView)
        self.contentView.addSubview(mainView)
    }
     //MARK: to set the constraints
    private func setupLayoutConstraints() {
        NSLayoutConstraint.activate([
            NSLayoutConstraint.init(item: mainView, attribute: .left, relatedBy: .equal, toItem: self.contentView, attribute: .left, multiplier: 1.0, constant: 8),
            NSLayoutConstraint.init(item: mainView, attribute: .right, relatedBy: .equal, toItem: self.contentView, attribute: .right, multiplier: 1.0, constant: -8),
            NSLayoutConstraint.init(item: mainView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1.0, constant: 4),
            NSLayoutConstraint.init(item: mainView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1.0, constant: -4)
            ])
        NSLayoutConstraint.activate([
            NSLayoutConstraint.init(item: titleLabel, attribute: .left, relatedBy: .equal, toItem: mainView, attribute: .left, multiplier: 1.0, constant: 8),
            NSLayoutConstraint.init(item: titleLabel, attribute: .right, relatedBy: .equal, toItem: mainView, attribute: .right, multiplier: 1.0, constant: -40),
            NSLayoutConstraint.init(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: mainView, attribute: .top, multiplier: 1.0, constant: 8),
            NSLayoutConstraint.init(item: titleLabel, attribute: .bottom, relatedBy: .equal, toItem: descLabel, attribute: .top, multiplier: 1.0, constant: -8)
            ])
        NSLayoutConstraint.activate([
            NSLayoutConstraint.init(item: descLabel, attribute: .left, relatedBy: .equal, toItem: titleLabel, attribute: .left, multiplier: 1.0, constant: 0),
            NSLayoutConstraint.init(item: descLabel, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1.0, constant: 8),
            NSLayoutConstraint.init(item: descLabel, attribute: .right, relatedBy: .equal, toItem: factImageView, attribute: .left, multiplier: 1.0, constant: -20),
            NSLayoutConstraint.init(item: descLabel, attribute: .bottom, relatedBy: .equal, toItem: mainView, attribute: .bottom, multiplier: 1.0, constant: -8)
            ])
        NSLayoutConstraint.activate([
            NSLayoutConstraint.init(item: factImageView, attribute: .left, relatedBy: .equal, toItem: descLabel, attribute: .right, multiplier: 1.0, constant: 20),
            NSLayoutConstraint.init(item: factImageView, attribute: .right, relatedBy: .equal, toItem: mainView, attribute: .right, multiplier: 1.0, constant: -50),
            NSLayoutConstraint.init(item: factImageView, attribute: .top, relatedBy: .equal, toItem: descLabel, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint.init(item: factImageView, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: mainView, attribute: .bottom, multiplier: 1.0, constant: -8),
            NSLayoutConstraint.init(item: factImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 130),
            NSLayoutConstraint.init(item: factImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 80)
            ])
        NSLayoutConstraint.activate([
            NSLayoutConstraint.init(item: nextArrowImageView, attribute: .left, relatedBy: .equal, toItem: titleLabel, attribute: .right, multiplier: 1.0, constant: 0),
            NSLayoutConstraint.init(item: nextArrowImageView, attribute: .top, relatedBy: .equal, toItem: mainView, attribute: .top, multiplier: 1.0, constant: 0),
            NSLayoutConstraint.init(item: nextArrowImageView, attribute: .bottom, relatedBy: .equal, toItem: mainView, attribute: .bottom, multiplier: 1.0, constant: 0),
            NSLayoutConstraint.init(item: nextArrowImageView, attribute: .right, relatedBy: .equal, toItem: mainView, attribute: .right, multiplier: 1.0, constant: -15)
            ])
    }
    public class func reuseIdentifier() -> String {
        return "FactsCell"
    }
}
