//
//  ViewController.swift
//  newsFeed
//
//  Created by pooja on 07/03/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import UIKit

class ViewController: UIViewController,NavBar {
    
    //MARK:- iVars
    var appConstants: AppConstants = AppConstants()
    var apiManager : ApiManager = ApiManager()
    lazy var tableView:UITableView = {
        let tv = UITableView.init(frame: CGRect.zero)
        tv.separatorStyle = .none
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.showsVerticalScrollIndicator = false
        tv.showsHorizontalScrollIndicator = false
        tv.delegate = self
        tv.dataSource = self
        tv.estimatedRowHeight = 150
        tv.register(FactsCell.self, forCellReuseIdentifier: FactsCell.reuseIdentifier())
        return tv
    }()
    var refreshControl:UIRefreshControl!
    var dataSource:FactsModel?
    fileprivate let apiUrl = AppConstants.BaseURL
    
    //MARK:- Overridden functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting up UI related stuff
        self.setNavigationBarProperties()
        self.setupUIElements()
        self.setupConstraints()
        
        //Adding refreshControl to tableView
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        refreshControl.beginRefreshing()
        
        //Calling API
        self.apiManager = ApiManager()
        apiManager.delegate = self
        self.apiManager.FetchDataFromApi(url: apiUrl)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private functions
    @objc private func refresh() {
        self.apiManager.FetchDataFromApi(url: apiUrl)
    }
    private func setupUIElements() {
        self.view.addSubview(tableView)
    }
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            NSLayoutConstraint.init(item: tableView, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: 0),
            NSLayoutConstraint.init(item: tableView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 4),
            NSLayoutConstraint.init(item: tableView, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: 0),
            NSLayoutConstraint.init(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0)
            ])
    }
}

extension ViewController: apiManagerDelegate {
    func apiSuccessResponse(_ response : Dictionary<String,Any>){
        if let source = self.dataSource {
            if source.dataSource.count > 0 {
                self.dataSource = nil
            }
        }
        self.dataSource = FactsModel.init(resp: response)
        guard let source = dataSource else { return }
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
            self.title = source.title
            self.tableView.reloadData()
        }
    }
    func apiFailureResponse(_ msgError: String) {
        if msgError == "Please try again" {
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let source = self.dataSource else {return 0}
        return source.dataSource.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FactsCell.reuseIdentifier(), for: indexPath) as? FactsCell else {return UITableViewCell()}
        cell.selectionStyle = .none
        cell.dataSource = self.dataSource?.dataSource[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
