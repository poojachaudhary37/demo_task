//
//  Extension+UIColor.swift
//  newsFeed
//
//  Created by pooja on 11/02/18.
//  Copyright © 2018 pooja. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class var navBarTintColor:UIColor{return UIColor.init(red: 197/255, green: 170/255, blue: 106/255, alpha: 1.0)}
    class var navBarTitleColor:UIColor{return UIColor.white}
    class var tableViewMainViewBackgroundColor:UIColor{return UIColor.init(red: 233/255, green: 233/255, blue: 233/255, alpha: 1.0)}
    class var titleLabelTextColor:UIColor{return UIColor.init(red: 70/255, green: 76/255, blue: 106/255, alpha: 1.0)}
}
